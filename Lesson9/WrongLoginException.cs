﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson9
{
    internal class WrongLoginException : Exception
    {
        public string Login { get; }

        public WrongLoginException()
        {

        }

        public WrongLoginException(string message, string login) : base(message)
        {
            Login = login;
        }
    }
}
