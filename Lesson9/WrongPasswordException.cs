﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson9
{
    internal class WrongPasswordException : Exception
    {
        public string Password { get; }

        public WrongPasswordException()
        {

        }

        public WrongPasswordException(string message, string password) : base(message)
        {
            Password = password;
        }
    }
}
