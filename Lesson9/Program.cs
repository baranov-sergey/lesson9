﻿using Lesson9;

internal class Program
{
    private static void Main(string[] args)
    {
        bool athorization = Athorization("James", "qwerty", "qwerty");
    }

    public static bool Athorization(string login, string password, string confirmPassword)
    {
        if (login.Length < 20)
        {
            foreach (char c in login)
            {
                if (c == ' ')
                {
                    throw new WrongLoginException("There are spaces in the login", login);
                }
            }
        }
        else
        {
            throw new WrongLoginException("Login length is more than 19 characters", login);
        }

        int cntDigit = default;
        if (password.Length < 20)
        {
            foreach (char c in password)
            {
                if (c == ' ')
                {
                    throw new WrongPasswordException("There are spaces in the password", password);
                }

                if (char.IsDigit(c))
                {
                    cntDigit++;
                }
            }

            if (cntDigit == 0)
            {
                throw new WrongPasswordException("There are no numbers in the password", password);
            }
        }
        else
        {
            throw new WrongPasswordException("Password length is more than 19 characters", password);
        }

        if (password == confirmPassword)
        {
            return true;
        }

        return false;
    }
}